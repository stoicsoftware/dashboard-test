define(["N/ui/serverWidget", "N/file"], function (ui, f) {

    /**
     * Module Description...
     *
     * @exports dashboard/pl
     *
     * @requires N/ui/serverWidget
     * @requires N/file
     *
     * @copyright 2016 Stoic Software
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType Portlet
     */
    var exports = {};

    /**
     * <code>render</code> event handler
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.portlet
     *        {Portlet} The portlet object used for rendering.
     * @param context.column
     *        {Number} The column index for the portlet on the
     *            dashboard. Use one of the following numeric values:
     *            <ol>
     *            <li>left column</li>
     *            <li>center column</li>
     *            <li>right column</li>
     *            </ol>
     *
     * @return {void}
     *
     * @static
     * @function render
     */
    function render(context) {
        var p = context.portlet;
        context.portlet.title = "d3 Test";
        context.portlet.clientScriptModulePath = "./d3Test.js";

        var field = p.addField({
            "id": "custpage_d3",
            "type": ui.FieldType.INLINEHTML
        });
        field.defaultValue = buildPortlet(loadStylesheet(), loadHtml());
    }

    function loadStylesheet() {
        return f.load({"id": "SuiteScripts/dashboard-portlet/d3.css"}).getContents();
    }

    function loadHtml() {
        return f.load({"id": "SuiteScripts/dashboard-portlet/pl_test.html"}).getContents();
    }

    function buildPortlet(style, html) {
        return html.replace("D3_STYLE", style);
    }
    exports.render = render;
    return exports;
});
