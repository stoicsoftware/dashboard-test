require.config({
    "paths": {
        "d3": "/SuiteScripts/lib/d3.min.js"
    },
    "shim": {
        "d3": {
            "exports": "d3"
        }
    }
});
define(["d3"], function (d3) {

    /**
     * Test d3
     *
     * @exports dashboard/d3
     *
     * @copyright 2016 Stoic Software
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType ClientScript
     */
    var exports = {};

    function pageInit() {
        var data = [2,4,7,25,38,97,15,28,63,43,79];

        var width = 420,
            barHeight = 20;

        var chart = d3.select("#d3-test-body")
            .attr("width", width)
            .attr("height", barHeight * data.length);

        var bar = chart.selectAll("g")
            .data(data)
            .enter().append("g")
                .attr("transform", function (d, i) { return "translate(0," + i * barHeight + ")"; });

        bar.append("rect")
            .attr("width", function (d) { return (linearScale(d) + "px"); })
            .attr("height", barHeight - 1);

        bar.append("text")
            .attr("x", function (d) { return (linearScale(d) - 3); })
            .attr("y", barHeight / 2)
            .attr("dy", "0.35em")
            .text(function (d) { return d; });
    }

    function linearScale(data) {
        return (data * 10);
    }

    exports.pageInit = pageInit;
    return exports;
});
